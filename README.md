# String search

> C++ programming exercise ([ESET C++ developer challenge](https://join.eset.com/en/challenges/c-developer))

## Project structure

### string_search
Command line processing, file search and results processing implementations

### string_search_lib
Implementation of search algorithms and factory function as a provider of search classes

### utils_lib
Various utility functions used throughout the project

### 3rd party libs
[Argh!](https://github.com/adishavit/argh) - Frustration-free command line processing

## Application usage

String search is a console application.

```
	<source_path> <pattern> [-sf|pf|pw]

	-s <source_path> -p <pattern> [-a <algorithm>] [-e <sampling_count>] [-j <tasks_count>] [-sf|pf|pw]

source_path
	file or directory to search in
	WARNINIG: Do not leave '\' at the end of a quoted path as it is considered as an escape sequence and parameters won't be processed correctly

pattern
	searched pattern

algorithm
	search algorithm, possible values are:
		"KMP" - Knuth-Morris-Pratt
		"BMH" (default) - Boyer-Moore-Horspool

sampling_count
	specifies how many times the search process should run
	default value is 1

tasks_count
	specifies number of concurrent task for -pf or -pw flags
	default value is 0, which means auto and should run one task for each CPU logical core

execution strategy flags:
	-sf (default) - serial files - runs one task for each file one by one
	-pf - parallel files - runs N tasks (specified by "-j" parameter) in parallel each for one file (process multiple files by N tasks)
	-pw - parallel windows - runs N tasks (specified by "-j" parameter) for each file processed one by one (process each file by N tasks)
```

It is possible to run application in three different modes:  
1. **sf flag** - uses one task to process files one by one - suitable for **single-core machine** or lower resources usage
2. **pf flag** - runs selected number of tasks which are running in parallel, each task is processing files from queue until all files are processed - suitable for **multiple files on multi-core machine**
3. **pw flag** - divides file into multiple parts and runs selected number of tasks, each task processing one of the part in parallel - suitable for **one file on multi-core machine**

At the end of a search there are printed out on standard output basic statistic along with search results.

## TODOs
- [ ] Tests

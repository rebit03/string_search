#include "stdafx.h"
#include "stat_utils.h"

#include <iomanip>
#include <sstream>

namespace stringSearch {
	namespace utils {
		namespace statistics {

			using namespace std;
			using namespace std::chrono;

			tuple<minutes::rep, seconds::rep, milliseconds::rep> breakDuration(duration_t dur)
			{
				auto min = duration_cast<minutes>(dur);
				dur -= min;
				auto sec = duration_cast<seconds>(dur);
				dur -= sec;
				auto ms = duration_cast<milliseconds>(dur);
				return { min.count(), sec.count(), ms.count() };
			}

			string durationToStr(duration_t dur)
			{
				auto[min, sec, ms] = breakDuration(dur);

				ostringstream str;
				str.fill('0');
				str <<
					std::setw(2) << min << ":" <<
					std::setw(2) << sec << ":" <<
					std::setw(3) << ms;

				return str.str();
			}

		}
	}
}

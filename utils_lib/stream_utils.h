#ifndef _H_461AD41A_1E0D_4BF3_908E_879FC4852F17_
#define _H_461AD41A_1E0D_4BF3_908E_879FC4852F17_

#include <istream>
#include <string>

namespace stringSearch {
	namespace utils {
		namespace stream {

			/**
			 * \brief Gets size of the stream
			 * \param[in] is Stream to get size of
			 * \return Size of the stream
			 */
			std::streamsize getStreamSize(std::istream& is);

			/**
			 * \brief Gets prefix
			 * \param[in] is Source stream
			 * \param[in] offset Offset within the stream to get prefix of
			 * \param[in] size Size of the prefix to get
			 * \return Prefix within the is file from offset position with max. length of size
			 */
			std::string getPrefix(std::istream& is, std::streampos offset, std::streamsize size);

			/**
			 * \brief Gets suffix
			 * \param[in] is Source stream
			 * \param[in] streamSize Size of the source stream
			 * \param[in] offset Offset within the stream to get suffix of
			 * \param[in] size Size of the suffix to get
			 * \return Suffix within the is file from offset position with max. length of size
			 */
			std::string getSuffix(std::istream& is, std::streamsize streamSize, std::streampos offset, std::streamsize size);

		}
	}
}

#endif // _H_461AD41A_1E0D_4BF3_908E_879FC4852F17_

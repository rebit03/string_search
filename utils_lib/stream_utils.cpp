#include "stdafx.h"
#include "stream_utils.h"
#include "string_utils.h"

#include <algorithm>

namespace stringSearch {
	namespace utils {
		namespace stream {

			using namespace std;

			streamsize getStreamSize(istream& is)
			{
				const streampos currentPos{ is.tellg() };
				const streamsize size{ is.seekg(0, ios::end).tellg() };
				is.seekg(currentPos);

				return size;
			};

			std::string getPrefix(istream& is, streampos offset, streamsize size)
			{
				const streamsize readSize{ min<streamsize>(offset, size) };

				if (0 < readSize) {
					is.seekg(offset - readSize);
					std::string buffer(readSize, 0);
					is.read(&buffer[0], readSize);
					return string::normalizeWhiteSpace(buffer);
				}
				else {
					return std::string();
				}
			};

			std::string getSuffix(istream& is, streamsize streamSize, streampos offset, streamsize size)
			{
				const streamsize readSize{ min((streamSize - offset), size) };

				if (0 != readSize) {
					is.seekg(offset);
					std::string buffer(readSize, 0);
					is.read(&buffer[0], readSize);
					return string::normalizeWhiteSpace(buffer);
				}
				else {
					return std::string();
				}
			};

		}
	}
}

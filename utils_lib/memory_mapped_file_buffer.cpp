#include "stdafx.h"
#include "memory_mapped_file_buffer.h"

#include "fs_utils.h"
#include "system_exception.h"

#include <algorithm>
#include <iostream>

namespace stringSearch {
	namespace fs {

		namespace {

			uint32_t getAllocationGranularity() noexcept
			{
				SYSTEM_INFO sysInfo;
				::GetSystemInfo(&sysInfo);
				return sysInfo.dwAllocationGranularity;
			}

		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		using namespace std;

		CMemMappedFileBuf::CMemMappedFileBuf(const char* filePath)
			: m_allocationGranularity(getAllocationGranularity())
		{
			try {
				m_fileHandle = ::CreateFile(
					filePath,
					GENERIC_READ,
					FILE_SHARE_READ,
					NULL,
					OPEN_EXISTING,
					FILE_ATTRIBUTE_NORMAL | FILE_FLAG_RANDOM_ACCESS,
					NULL
				);

				if (INVALID_HANDLE_VALUE == m_fileHandle) {
					throw exceptions::CSystemException("Failed to open file");
				}

				m_fileSize = utils::fs::getFileSize(m_fileHandle);

				m_fileMappingHandle = ::CreateFileMapping(
					m_fileHandle,
					NULL,
					PAGE_READONLY,
					0,
					0,
					NULL
				);

				if (NULL == m_fileMappingHandle) {
					throw exceptions::CSystemException("Failed to create file mapping");
				}
			}
			catch (...) {
				close();
				throw;
			}
		}

		CMemMappedFileBuf::~CMemMappedFileBuf()
		{
			close();
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		void CMemMappedFileBuf::close() noexcept
		{
			if (NULL != m_mappedData) {
				::UnmapViewOfFile(m_mappedData);
				m_mappedData = NULL;
			}
			if (NULL != m_fileMappingHandle) {
				::CloseHandle(m_fileMappingHandle);
				m_fileMappingHandle = NULL;
			}
			if (INVALID_HANDLE_VALUE != m_fileHandle) {
				::CloseHandle(m_fileHandle);
				m_fileHandle = INVALID_HANDLE_VALUE;
			}
		}

		void CMemMappedFileBuf::mapView(std::streampos offset)
		{
			// mapping has to be aligned with allocation granularity
			const uint64_t alignedOffset{ static_cast<uint64_t>(offset / m_allocationGranularity * m_allocationGranularity) };
			const uint32_t offsetLow{ static_cast<uint32_t>(alignedOffset) };
			const uint32_t offsetHigh{ static_cast<uint32_t>(alignedOffset >> 32) };
			const uint32_t sizeToMap = static_cast<uint32_t>(min(m_allocationGranularity, (m_fileSize - alignedOffset)));

			m_mappedData = static_cast<streambuf::char_type*>(::MapViewOfFile(
				m_fileMappingHandle,
				FILE_MAP_READ,
				offsetHigh,
				offsetLow,
				sizeToMap
			));

			if (NULL == m_mappedData) {
				unmapView();
				throw exceptions::CSystemException("Failed to map view of file");
			}

			m_mappedOffset = alignedOffset;
			m_mappedDataSize = sizeToMap;
		}

		void CMemMappedFileBuf::unmapView() noexcept
		{
			::UnmapViewOfFile(m_mappedData);
			m_mappedData = NULL;
			m_mappedOffset = 0;
			m_mappedDataSize = 0;
		}

		void CMemMappedFileBuf::updateGetArea() noexcept
		{
			if (NULL != m_mappedData) {
				if (m_fileOffset < m_mappedOffset || m_fileOffset >= (m_mappedOffset + m_mappedDataSize)) {
					// file offset is out of currently mapped area
					unmapView();
					setg(nullptr, nullptr, nullptr);
				}
				else {
					const uint32_t mappedViewOffset{ static_cast<uint32_t>(m_fileOffset % m_allocationGranularity) };
					setg(m_mappedData, m_mappedData + mappedViewOffset, m_mappedData + m_mappedDataSize);
				}
			}
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		streampos CMemMappedFileBuf::seekoff(streamoff off, ios::seekdir dir, ios::openmode which/* = ios::in*/)
		{
			streampos pos{ 0 };

			switch (dir) {
				default:
				case ios::beg: {
					pos = off;
				} break;
				case ios::cur: {
					if (NULL != m_mappedData)
					{	// when some data are mapped, the actual position has to be updated according to streambuf::gptr() (position in the get area)
						m_fileOffset = m_mappedOffset + (gptr() - m_mappedData);
					}
					pos = m_fileOffset + off;
				} break;
				case ios::end: {
					pos = m_fileSize + off;
				} break;
			}

			return seekpos(pos);
		}

		streampos CMemMappedFileBuf::seekpos(streampos pos, ios::openmode which/* = std::ios::in*/)
		{
			if (pos < 0) {
				pos = 0;
			}
			else if (pos > m_fileSize) {
				pos = m_fileSize;
			}

			m_fileOffset = pos;

			updateGetArea();

			return pos;
		}

		char_traits<streambuf::char_type>::int_type CMemMappedFileBuf::underflow()
		{
			seekoff(0, ios::cur); // to align m_fileOffset with streambuf get area and mapped area

			if (m_fileOffset == m_fileSize) {
				return traits_type::eof();
			}

			const uint32_t mappedViewOffset{ static_cast<uint32_t>(m_fileOffset % m_allocationGranularity) };

			if (NULL == m_mappedData) {
				mapView(m_fileOffset);
				setg(m_mappedData, m_mappedData + mappedViewOffset, m_mappedData + m_mappedDataSize);
			}

			const streambuf::char_type c{ *(m_mappedData + mappedViewOffset) };

			return traits_type::to_int_type(c);
		}

	}
}

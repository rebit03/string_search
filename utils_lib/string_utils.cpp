#include "stdafx.h"
#include "string_utils.h"

namespace stringSearch {
	namespace utils {
		namespace string {

			std::string normalizeWhiteSpace(const std::string& str)
			{
				std::string normalized;
				for (const auto& c : str)
				{
					if (0 == std::strncmp(&c, "\t", 1)) {
						normalized.append("\\t");
					}
					else if (0 == std::strncmp(&c, "\r", 1)) {
						normalized.append("\\r");
					}
					else if (0 == std::strncmp(&c, "\n", 1)) {
						normalized.append("\\n");
					}
					else {
						normalized.push_back(c);
					}
				}
				return normalized;
			}

		}
	}
}

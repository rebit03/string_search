#ifndef _H_CF6F8EE2_551C_4F06_9A40_830C8149CB53_
#define _H_CF6F8EE2_551C_4F06_9A40_830C8149CB53_

#include <chrono>
#include <string>
#include <tuple>
#include <type_traits>
#include <utility>

namespace stringSearch {
	namespace utils {
		namespace statistics {

			/**
			 * \brief Type of measured intervals
			 */
			using duration_t = std::chrono::steady_clock::duration;

			/**
			 * \brief Measure execution time of function without return value /void(Args...)/
			 * \param[in] func Function to measure
			 * \param[in] args Measured function parameters
			 * \return duration_t execution time
			 */
			template <typename FunctionT, typename ...Args>
			typename std::enable_if_t<std::is_function_v<FunctionT> && std::is_void_v<std::invoke_result_t<FunctionT, Args...>>, duration_t>
			duration(FunctionT&& func, Args&&... args)
			{
				const std::chrono::steady_clock::time_point start{ std::chrono::steady_clock::now() };
				func(std::forward<Args>(args)...);
				return (std::chrono::steady_clock::now() - start);
			}

			/**
			 * \brief Measure execution time of function with return value /resultT(Args...)/
			 * \param[in] func Function to measure
			 * \param[in] args Measured function parameters
			 * \return Pair of duration_t execution time and result of measured function
			 */
			template <typename FunctionT, typename ...Args>
			typename std::enable_if_t<std::is_function_v<FunctionT> && !std::is_void_v<std::invoke_result_t<FunctionT, Args...>>, std::pair<duration_t, std::invoke_result_t<FunctionT, Args...>>>
			duration(FunctionT&& func, Args&&... args)
			{
				using resultT = std::invoke_result_t<FunctionT, Args...>;

				const std::chrono::steady_clock::time_point start{ std::chrono::steady_clock::now() };
				resultT res{ func(std::forward<Args>(args)...) };
				duration_t fnDuration{ (std::chrono::steady_clock::now() - start) };

				return std::make_pair<duration_t, resultT>(move(fnDuration), move(res));
			}

			/**
			 * \brief Break duration time into minutes, seconds and milliseconds
			 * \param[in] dur Time to break
			 * \return Tuple of values <min, sec, ms>
			 */
			std::tuple<std::chrono::minutes::rep, std::chrono::seconds::rep, std::chrono::milliseconds::rep>
			breakDuration(duration_t dur);

			/**
			* \brief Convert duration time into string representation
			* \param[in] dur Time to convert
			* \return String in min:sec:ms format
			*/
			std::string durationToStr(duration_t dur);

		}
	}
}

#endif // _H_CF6F8EE2_551C_4F06_9A40_830C8149CB53_

#include "stdafx.h"
#include "fs_utils.h"
#include "system_exception.h"

#include <filesystem>

namespace stringSearch {
	namespace utils {
		namespace fs {

			using namespace std;
			using namespace std::experimental::filesystem;

			namespace {

				bool isDirectory(const char* path)
				{
					return (0 != (::GetFileAttributes(path) & FILE_ATTRIBUTE_DIRECTORY));
				}

			}

			filesList_t getFilesList(const string& src)
			{
				/* NOTE: do not use std::experimental::filesystem::is_directory()
				 * it does not work with paths with '.' in the name - automatically considers them files
				 */
				filesList_t files;
				if (isDirectory(src.c_str())) {
					for (const auto& entry : recursive_directory_iterator(src/*, directory_options::skip_permission_denied - NOT AVAILABLE*/))
					{
						string path{ entry.path()/*.lexically_normal() - NOT AVAILABLE*/.string() };
						if (!isDirectory(path.c_str())) {
							files.push_back(path);
						}
					}
				}
				else {
					files.push_back(src);
				}
				return files;
			}

			int64_t getFileSize(const char* filePath)
			{
				// NOTE: do not use ::GetFileAttributesEx it does not return correct file size

				HANDLE fileHandle = ::CreateFile(
					filePath,
					GENERIC_READ,
					FILE_SHARE_READ,
					NULL,
					OPEN_EXISTING,
					FILE_ATTRIBUTE_NORMAL,
					NULL
				);

				if (INVALID_HANDLE_VALUE == fileHandle) {
					throw exceptions::CSystemException("Failed to open file");
				}
				else {
					int64_t size = getFileSize(fileHandle);
					::CloseHandle(fileHandle);
					return size;
				}
			}

			int64_t getFileSize(HANDLE fileHandle)
			{
				LARGE_INTEGER size;
				if (0 == ::GetFileSizeEx(fileHandle, &size)) {
					throw exceptions::CSystemException("Failed to get file size");
				}
				else {
					return size.QuadPart;
				}
			}

		}
	}
}

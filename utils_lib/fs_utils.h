#ifndef _H_2B52083F_6821_4B8E_9407_108B6A6499AF_
#define _H_2B52083F_6821_4B8E_9407_108B6A6499AF_

#include <vector>
#include <string>
#include <Windows.h>

namespace stringSearch {
	namespace utils {
		namespace fs {

			/**
			 * \brief Collection of file paths
			 */
			using filesList_t = std::vector<std::string>;

			/**
			 * \brief Recursively traverse filesystem and collect all files in src
			 * \param[in] src File or source directory to traverse
			 * \return Collection of found files
			 */
			filesList_t getFilesList(const std::string& src);

			/**
			 * \brief Get file size based on a file path
			 * \param[in] filePath Path to the file
			 * \return Size of the file
			 */
			int64_t getFileSize(const char* filePath);

			/**
			 * \brief Get file size based on a file handle
			 * \param[in] filePath Path to the file
			 * \return Size of the file
			 */
			int64_t getFileSize(HANDLE fileHandle);

		}
	}
}

#endif // _H_2B52083F_6821_4B8E_9407_108B6A6499AF_

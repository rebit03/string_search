#ifndef _H_F84A539E_6CEA_410E_8ABD_B7F79AD1176F_
#define _H_F84A539E_6CEA_410E_8ABD_B7F79AD1176F_

#include "memory_mapped_file_buffer.h"
#include <istream>

namespace stringSearch{
	namespace fs {

		/**
		 * std::istream using memory mapped file buffer
		 */
		class CMemMappedFileStream : public std::istream
		{
		public:
			CMemMappedFileStream(const char* filePath)
				: m_fileBuffer(filePath)
				, std::istream(&m_fileBuffer)
			{}
		private:
			CMemMappedFileBuf	m_fileBuffer;	//!< memory mapped file buffer
		};

	}
}

#endif // _H_F84A539E_6CEA_410E_8ABD_B7F79AD1176F_

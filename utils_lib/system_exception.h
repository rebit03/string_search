#ifndef _H_9408119C_BD4A_4C9D_81CB_32BA9FEC5298_
#define _H_9408119C_BD4A_4C9D_81CB_32BA9FEC5298_

#include <system_error>
#include <Windows.h>

namespace stringSearch {
	namespace exceptions {

		/**
		 * Windows exception getting error code from \'GetLastError\' function
		 */
		class CSystemException : public std::system_error
		{
		public:
			explicit CSystemException(const char* what)
				: system_error(::GetLastError(), std::system_category(), what)
			{}
		};

	}
}

#endif // _H_9408119C_BD4A_4C9D_81CB_32BA9FEC5298_

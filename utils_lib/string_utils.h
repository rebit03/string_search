#ifndef _H_A5C016D0_99E7_431A_B83E_F1627A955AA5_
#define _H_A5C016D0_99E7_431A_B83E_F1627A955AA5_

#include <string>

namespace stringSearch {
	namespace utils {
		namespace string {

			/**
			 * \brief Convert white space within the input string to escaped sequences
			 * \param[in] str String to normalize
			 * \return String with white spaces converted to escape sequences
			 */
			std::string normalizeWhiteSpace(const std::string& str);

			/**
			 * \brief Case insensitive string comparison
			 */
			struct icmp {
				bool operator()(const std::string& lhs, const std::string& rhs) const {
					return 0 > ::_stricmp(lhs.c_str(), rhs.c_str());
				}
			};

		}
	}
}

#endif // _H_A5C016D0_99E7_431A_B83E_F1627A955AA5_

#ifndef _H_9D844126_CB82_4016_B152_A422BD4315F2_
#define _H_9D844126_CB82_4016_B152_A422BD4315F2_

#include <ios>
#include <streambuf>

#include <windows.h>

namespace stringSearch {
	namespace fs {

		/**
		 * Implementation of std::streambuf core interface for Windows memory mapped file
		 */
		class CMemMappedFileBuf : public std::streambuf
		{
		public:
			explicit CMemMappedFileBuf(const char* filePath);
			CMemMappedFileBuf(const CMemMappedFileBuf& other) = delete;
			CMemMappedFileBuf& operator=(const CMemMappedFileBuf& other) = delete;
			~CMemMappedFileBuf();
		protected:
			// Positioning
			std::streampos seekoff(std::streamoff off, std::ios::seekdir dir, std::ios::openmode which = std::ios::in) override;
			std::streampos seekpos(std::streampos pos, std::ios::openmode which = std::ios::in) override;
			// Get area
			traits_type::int_type underflow() override;
		private:
			/**
			 * \brief Updates memory mapped view according to the given offset
			 * \param[in] offset Offset within the file
			 */
			void mapView(std::streampos offset);

			/**
			 * \brief Unmap currently mapped view and reset related properties
			 */
			void unmapView() noexcept;

			/**
			 * \brief Updates get area of std::streambuf by calling std::streambuf::setg according to actually mapped area and file offset
			 */
			void updateGetArea() noexcept;

			/**
			 * \brief Unmap view, close handles
			 */
			void close() noexcept;
		private:
			const uint32_t				m_allocationGranularity;				//!< system allocation granularity
			HANDLE						m_fileHandle{ INVALID_HANDLE_VALUE };	//!< HANDLE to the opened file
			HANDLE						m_fileMappingHandle{ NULL };			//!< HANDLE to the mapped view
			std::streamsize				m_fileSize{ 0 };						//!< size of the opened file
			std::streamoff				m_fileOffset{ 0 };						//!< current offset within the opened file
																				//!< may not be actual when some data are mapped
																				//!< use gptr() to get actual position within the mapped data (streambuf get area)
			std::streamoff				m_mappedOffset{ 0 };					//!< offset of the mapped view
			std::streamsize				m_mappedDataSize{ 0 };					//!< size of the mapped view
			std::streambuf::char_type*	m_mappedData{ NULL };					//!< mapped view data
		};

	}
}

#endif // _H_9D844126_CB82_4016_B152_A422BD4315F2_

#ifndef _H_19D0F762_A611_48FE_B186_733672C0BEF2_
#define _H_19D0F762_A611_48FE_B186_733672C0BEF2_

namespace stringSearch {

	struct SSearchParams;	// fwd declaration
	/**
	 * \brief File search
	 * \param[in] params Parameters for search process
	 */
	void fileSearch(SSearchParams params);

}

#endif // _H_19D0F762_A611_48FE_B186_733672C0BEF2_

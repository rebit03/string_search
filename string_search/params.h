#ifndef _H_99E7C25C_4D04_470B_84F9_60181F21B50F_
#define _H_99E7C25C_4D04_470B_84F9_60181F21B50F_

#include "types.h"
#include "utils.h"

#include <string_search_lib\types.h>

namespace stringSearch {

	/**
	 * Structure representing parameters combination to execute search with
	 */
	struct SSearchParams
	{
		SSearchParams() = default;
		SSearchParams(
			const std::string& source,
			const std::string& pattern,
			const std::string& algorithm = "",
			const std::string& executionStrategy = "",
			uint32_t tasksCount = 0,
			uint32_t sampling = 1
		)
			: sourcePath(source)
			, searchedPattern(pattern)
			, concurrentTasksCount(tasksCount)
			, sampling(sampling)
			, searchAlgorithm(utils::strToAlgorithmType(algorithm))
			, executionStrategy(utils::strToExecutionStrategy(executionStrategy))
		{
		}

		std::string			sourcePath;					//!< source file or directory to search in
		std::string			searchedPattern;			//!< pattern to search for
		ESearchAlgorithm	searchAlgorithm{ ESearchAlgorithm::default };		//!< type of search algorithm to use for search process
		EExecutionStrategy	executionStrategy{ EExecutionStrategy::default };	//!< type of execution strategy to use for search process
		uint32_t			concurrentTasksCount{ 0 };	//!< number of parallel tasks to run /0 - auto, 1 - serial execution/
		uint32_t			sampling{ 1 };				//!< how many times the search will be repeated
	};

}

#endif // _H_99E7C25C_4D04_470B_84F9_60181F21B50F_

#include "stdafx.h"
#include "file_search.h"
#include "params.h"

#include <string_search_lib\search.h>
#include <utils_lib\fs_utils.h>
#include <utils_lib\memory_mapped_file_stream.h>
#include <utils_lib\stat_utils.h>
#include <utils_lib\stream_utils.h>

#include <atomic>
#include <exception>
#include <future>
#include <iomanip>
#include <iostream>
#include <optional>
#include <type_traits>
#include <unordered_map>

namespace stringSearch {

	using namespace std;

	namespace fsUtils = utils::fs;;
	namespace statUtils = utils::statistics;
	namespace streamUtils = utils::stream;

	namespace {

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// DATA TYPES

		/**
		 * Structure representing data of one file search
		 */
		struct SFileSearchData {
			int64_t				fileSize{ 0 };	//!< size of searched file
			searchResults_t		searchResults;	//!< results of search process
		};

		/**
		 * Structure representing data of one file search
		 */
		struct SMeasuredFileSearchData {
			statUtils::duration_t	runningTime{ 0 };	//!< duration of search process
			SFileSearchData			data;				//!< results of search process
		};

		/**
		 * Collection of search results pair<file_path, data>
		 */
		using measuredFileSearchResults_t = unordered_map<string, SMeasuredFileSearchData>;

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// RESULTS PROCESSING

		const double MB = 1024 * 1024;

		double getProcessingSpeed(const measuredFileSearchResults_t& results, const statUtils::duration_t& execTime)
		{
			int64_t filesSize{ 0 };
			for (const auto& res : results) { // accumulate size of files and their processed time
				filesSize += res.second.data.fileSize;
			}

			int64_t ms = chrono::duration_cast<chrono::milliseconds>(execTime).count(); // total processed time in ms
			if (0 < ms) {
				return (filesSize / static_cast<double>(ms)) * 1000 / MB; // processing speed in MB/s
			}
			else {
				return 0;
			}
		}

		void printOverallInfo(const measuredFileSearchResults_t& results, const statUtils::duration_t& execTime, uint32_t sampling, uint32_t tasksCount)
		{
			// overall info - running_time, sampling, processed_files_count, tasks_count, processing_speed
			if (sampling > 1) {
				cout << "average ";
			}
			cout << "running time [min:sec:ms]: " << statUtils::durationToStr(execTime / sampling).c_str() << endl;

			cout << "sampling: " << sampling << endl;

			cout << "processed files: " << results.size() << endl;

			cout << "number of tasks: " << tasksCount << endl;

			if (sampling > 1) {
				cout << "average ";
			}
			cout << "processing speed: ";
			double speed = getProcessingSpeed(results, execTime / sampling);
			if (0.001 <= speed) {
				cout << fixed << setprecision(3) << speed << " [MB/s]" << endl;
			}
			else {
				cout << "unknown" << endl;
			}
		}

		void printFilesInfo(const measuredFileSearchResults_t& results, uint32_t sampling)
		{
			// file_name | results_count | file_size | running_time
			cout <<
				"file path | results count | file size [MB] | " <<
				((sampling > 1) ? "average " : "") << "processing time [min:sec:ms]" << endl << endl;

			for (const auto& res : results)
			{
				const SMeasuredFileSearchData& result{ res.second };

				cout <<
					res.first << "' | " <<
					result.data.searchResults.size() << " | " <<
					fixed << setprecision(4) << (result.data.fileSize / MB) << " | " <<
					statUtils::durationToStr(result.runningTime / sampling).c_str() << endl;
			}
		}

		void printSearchResults(const measuredFileSearchResults_t& results)
		{
			// all results
			for (const auto& res : results) {
				const searchResults_t& fileResult{ res.second.data.searchResults };
				for (const auto& result : fileResult) {
					cout << res.first << "(" << result.offset << "):" << result.prefix << "..." << result.suffix << endl;
				}
			}
		}

		void printResults(const measuredFileSearchResults_t& results, const statUtils::duration_t& execTime, uint32_t sampling, uint32_t tasksCount)
		{
			cout << "OVERALL STATISTICS" << endl;
			printOverallInfo(results, execTime, sampling, tasksCount);
			cout << endl << endl;
			cout << "FILES STATISTICS" << endl;
			printFilesInfo(results, sampling);
			cout << endl << endl;
			cout << "SEARCH RESULTS" << endl;
			printSearchResults(results);
			cout << endl << endl;
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// BASIC SEARCH FUNCTIONS

		/**
		 * \brief Perform file search
		 * \param[in] searchClass Search class implementing search algorithm
		 * \param[in] filePath Path to the file to process
		 * \return Collection of search results
		 */
		SFileSearchData fileSearch(ISearch& searchClass, const string& filePath)
		{
			fs::CMemMappedFileStream ifs(filePath.c_str());
			return SFileSearchData{ streamUtils::getStreamSize(ifs), searchClass.search(ifs) };
		}

		/**
		 * \brief Perform file search and measure it's running time
		 * \param[in] searchClass Search class implementing search algorithm
		 * \param[in] filePath Path to the file to process
		 * \return Collection of search result data
		 */
		SMeasuredFileSearchData measuredFileSearch(ISearch& searchClass, const string& filePath)
		{
			using searchFnT = SFileSearchData(ISearch&, const string&);
			auto[execTime, results] = statUtils::duration<searchFnT>(fileSearch, searchClass, filePath);
			return SMeasuredFileSearchData{ move(execTime), move(results) };
		}

		/**
		 * \brief Perform file search in given range
		 * \param[in] searchClass Search class implementing search algorithm
		 * \param[in] filePath Path to the file to process
		 * \param[in] offset Offset within the file to start search at
		 * \param[in] size Size of part to search at
		 * \return Collection of search results
		 */
		SFileSearchData fileSearch(ISearch& searchClass, const string& filePath, int64_t offset, int64_t size)
		{
			fs::CMemMappedFileStream ifs(filePath.c_str());
			int64_t fileSize{ streamUtils::getStreamSize(ifs) };
			ifs.seekg(offset);

			return SFileSearchData{ fileSize, searchClass.searchRange(ifs, size) };
		}

		void handleFileSearchException(measuredFileSearchResults_t& results, const string& filePath)
		{
			try {
				throw;
			}
			catch (const std::exception& e) {
				cerr << "Failed to process file \"" << filePath.c_str() << "\": " << e.what() << endl;
				results.emplace(make_pair(filePath, SMeasuredFileSearchData())); // insert empty results
			}
			catch (...) {
				cerr << "Failed to process file \"" << filePath.c_str() << "\": Unknown exception" << endl;
				results.emplace(make_pair(filePath, SMeasuredFileSearchData())); // insert empty results
			}
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// -sf SERIAL FILES SEARCH

		namespace sf {

			/**
			 * \brief Process files one by one
			 * \param[in] searchClass Search class implementing search algorithm
			 * \param[in] files List of files to process
			 * \return Collection of search process results
			 */
			measuredFileSearchResults_t serialFilesSearch(ISearch& searchClass, const fsUtils::filesList_t& files)
			{
				measuredFileSearchResults_t results;
				
				for (const auto& file : files) {
					try {
						results.emplace(make_pair(file, measuredFileSearch(searchClass, file)));
					}
					catch (...) {
						handleFileSearchException(results, file);
					}
				}

				return results;
			}

		} // namespace sf

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// -pw PARALLEL WINDOWS SEARCH

		namespace pw {

			/**
			 * \brief Split file to multiple parts, run multiple tasks, each is processing one part of the file
			 * \param[in] searchClass Search class implementing search algorithm
			 * \param[in] filePath Path to the file to process
			 * \param[in] patternSize Size of the search pattern
			 * \param[in] tasksCount Number of parallel tasks to run
			 * \return Collection of search process results
			 */
			SFileSearchData parallelWindowsSearch(ISearch& searchClass, const string& filePath, size_t patternSize, uint32_t tasksCount)
			{
				const size_t overlapSize{ patternSize - 1 }; // windows overlap size

				const auto[windowSize, largerWindowsCount] = [&] {
					const int64_t overlapOverhead{ static_cast<int64_t>((tasksCount - 1) * overlapSize) };
					const int64_t sizeToSplit{ fsUtils::getFileSize(filePath.c_str()) + overlapOverhead };

					// [largerWindowsCount]
					// because when the sizeToSplit is not precisely divisible by the tasksCount, some windows has to be larger so the whole file is processed
					return make_pair<int64_t, uint32_t>((sizeToSplit / tasksCount), (sizeToSplit % tasksCount));
				}();

				vector<future<SFileSearchData>> tasks;
				int64_t offset{ 0 }; // offset within file
				for (uint32_t ti{ 0 }; ti < tasksCount; ++ti)
				{	// split file to 'tasksCount' parts and run async task for each part
					int64_t size{ windowSize };
					if (ti < largerWindowsCount) {
						size += 1; // process also bytes from (sizeToSplit % tasksCount)
					}

					using searchFnT = SFileSearchData(ISearch&, const string&, int64_t, int64_t);
					tasks.push_back(async<searchFnT>(launch::async, fileSearch, ref(searchClass), cref(filePath), offset, size));

					offset += size - overlapSize; // move to next part
				}

				SFileSearchData results;

				for (auto& t : tasks)
				{	// wait for results and aggregate
					SFileSearchData& taskResults{ t.get() };

					results.fileSize = taskResults.fileSize;
					results.searchResults.insert(
						results.searchResults.end(),
						make_move_iterator(taskResults.searchResults.begin()),
						make_move_iterator(taskResults.searchResults.end())
					);
				}

				return results;
			}

			/**
			 * \brief Process files one at the time, each processed by multiple parallel tasks
			 * \param[in] searchClass Search class implementing search algorithm
			 * \param[in] files List of files to process
			 * \param[in] patternSize Size of the search pattern
			 * \param[in] tasksCount Number of parallel tasks to run
			 * \return Collection of search process results
			 */
			measuredFileSearchResults_t parallelWindowsSearch(ISearch& searchClass, const fsUtils::filesList_t& files, size_t patternSize, uint32_t tasksCount)
			{
				measuredFileSearchResults_t results;

				for (const auto& file : files) {
					try {
						using searchFnT = SFileSearchData(ISearch&, const string&, size_t, uint32_t);
						auto[execTime, fileResults] = statUtils::duration<searchFnT>(parallelWindowsSearch, searchClass, file, patternSize, tasksCount);
						results.emplace(make_pair(file, SMeasuredFileSearchData{ move(execTime), move(fileResults) }));
					}
					catch (...) {
						handleFileSearchException(results, file);
					}
				}

				return results;
			}
		
		} // namespace pw

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// -pf PARALLEL FILES SEARCH

		namespace pf {

			/**
			 * Thread safe index provider
			 */
			class CIndex_mt {
			public:
				explicit CIndex_mt(size_t indexLimit)
					: m_limit(indexLimit)
				{}

				/**
				 * \brief Get next index
				 * \return Next index in line when limit was not reached, nullopt otherwise
				 */
				optional<size_t> getNext() {
					size_t index{ atomic_fetch_add(&m_index, 1) };
					if (index < m_limit) {
						return make_optional(index);
					}
					else {
						return nullopt;
					}
				}

			private:
				const size_t	m_limit{ 0 };	//!< Max. index (exclusive)
				atomic_size_t	m_index{ 0 };	//!< Current index
			};

			/**
			 * \brief Get index, get file at given index, perform search process, repeat until all files are processed
			 * \param[in] searchClass Search class implementing search algorithm
			 * \param[in] files List of files to process
			 * \param[in] index Thread safe index provider
			 * \return Collection of search process results
			 */
			measuredFileSearchResults_t greedyFilesSearch(ISearch& searchClass, const fsUtils::filesList_t& files, CIndex_mt& index)
			{
				measuredFileSearchResults_t results;

				while (true) {
					optional<size_t> maybeIndex{ index.getNext() };
					if (maybeIndex.has_value()) { // got index? -> process file
						const auto& filePath{ files.at(maybeIndex.value()) };
						try {
							results.emplace(make_pair(filePath, measuredFileSearch(searchClass, filePath)));
						}
						catch (...) {
							handleFileSearchException(results, filePath);
						}
					}
					else {
						break; // no more files
					}
				}

				return results;
			}

			/**
			 * \brief Run multiple parallel tasks, each is processing one file at the time and when finished it tries to process next file in the list
			 * \param[in] searchClass Search class implementing search algorithm
			 * \param[in] files List of files to process
			 * \param[in] tasksCount Number of parallel tasks to run
			 * \return Collection of search process results
			 */
			measuredFileSearchResults_t parallelFilesSearch(ISearch& searchClass, const fsUtils::filesList_t& files, uint32_t tasksCount)
			{
				CIndex_mt index(files.size());

				vector<future<measuredFileSearchResults_t>> tasks;
				for (uint32_t n{ 0 }; n < tasksCount; ++n) { // launch N parallel task each getting files from files list
					tasks.push_back(async(launch::async, greedyFilesSearch, ref(searchClass), cref(files), ref(index)));
				}
				
				measuredFileSearchResults_t results;
				for (auto& t : tasks) { // wait for results and aggregate them to one collection
					auto& taskResults{ t.get() };
					results.insert(make_move_iterator(taskResults.begin()), make_move_iterator(taskResults.end()));
				}

				return results;
			}

		} // namespace pf

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		measuredFileSearchResults_t filesSearch(const fsUtils::filesList_t& files, const SSearchParams& params)
		{
			auto searchClass{ getSearchClass(params.searchAlgorithm, params.searchedPattern) };
			ISearch& searchClassRef{ *(searchClass.get()) };

			auto searchFn = [&] {
				switch (params.executionStrategy) {
					default:
					case EExecutionStrategy::serialFiles:
						return sf::serialFilesSearch(searchClassRef, files);
					case EExecutionStrategy::parallelFiles:
						return pf::parallelFilesSearch(searchClassRef, files, params.concurrentTasksCount);
					case EExecutionStrategy::parallelWindows:
						return pw::parallelWindowsSearch(searchClassRef, files, params.searchedPattern.size(), params.concurrentTasksCount);
				}
			};

			if (1 == params.sampling) {
				return searchFn();
			}
			else {
				measuredFileSearchResults_t results;
				for (uint32_t i{ 0 }; i < params.sampling; ++i) {
					measuredFileSearchResults_t taskResults{ searchFn() };
					// aggregate results from each execution
					for (auto& res : taskResults) {
						if (results.find(res.first) != results.end()) {
							// search results should be the same so just accumulate the time
							results.at(res.first).runningTime += res.second.runningTime;
						}
						else {
							// first results, insert including search results
							results.emplace(move(res));
						}
					}
				}
				return results;
			}
		}

	} // namespace

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void fileSearch(SSearchParams params)
	{
		fsUtils::filesList_t files{ fsUtils::getFilesList(params.sourcePath) };

		if (!files.empty()) {
			if (0 == params.concurrentTasksCount)
			{	// auto mode
				params.concurrentTasksCount = thread::hardware_concurrency();
			}

			if ((EExecutionStrategy::parallelFiles == params.executionStrategy) && (files.size() < static_cast<size_t>(params.concurrentTasksCount)))
			{	// no need to run more tasks than available files
				params.concurrentTasksCount = static_cast<uint32_t>(files.size());
			}

			if (1 == params.concurrentTasksCount)
			{	// no need to try parallel execution strategy for just one task
				params.executionStrategy = EExecutionStrategy::serialFiles;
			}

			using searchFnT = measuredFileSearchResults_t(const fsUtils::filesList_t&, const SSearchParams&);
			auto[execTime, results] = statUtils::duration<searchFnT>(filesSearch, files, params);

			printResults(results, execTime, params.sampling, params.concurrentTasksCount);
		}
		else {
			printResults(measuredFileSearchResults_t(), statUtils::duration_t(0), params.sampling, params.concurrentTasksCount);
		}
	}

}

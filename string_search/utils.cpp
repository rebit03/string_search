#include "stdafx.h"
#include "utils.h"

namespace stringSearch {
	namespace utils {

		ESearchAlgorithm strToAlgorithmType(const std::string& str)
		{
			const auto& algorithm{ strToAlgorithmMapping.find(str) };
			if (algorithm != strToAlgorithmMapping.end()) {
				return algorithm->second;
			}
			else {
				return ESearchAlgorithm::default;
			}
		}

		bool strToAlgorithmType(ESearchAlgorithm& type, const std::string& str)
		{
			const auto& algorithm{ strToAlgorithmMapping.find(str) };

			if (algorithm != strToAlgorithmMapping.end()) {
				type = algorithm->second;
				return true;
			}
			else {
				return false;
			}
		}

		EExecutionStrategy strToExecutionStrategy(const std::string& str)
		{
			const auto& exec{ strToExecutionStrategyMapping.find(str) };
			if (exec != strToExecutionStrategyMapping.end()) {
				return exec->second;
			}
			else {
				return EExecutionStrategy::default;
			}
		}

		bool strToExecutionStrategy(EExecutionStrategy& strategy, const std::string& str)
		{
			const auto& exec{ strToExecutionStrategyMapping.find(str) };

			if (exec != strToExecutionStrategyMapping.end()) {
				strategy = exec->second;
				return true;
			}
			else {
				return false;
			}
		}

	}
}

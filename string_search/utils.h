#ifndef _H_5AC1F24D_5758_4DE7_AABA_21773F9B354A_
#define _H_5AC1F24D_5758_4DE7_AABA_21773F9B354A_

#include "types.h"

#include <string_search_lib\types.h>
#include <utils_lib\string_utils.h>

#include <map>
#include <utility>
#include <string>

namespace stringSearch {
	namespace utils {

		/**
		 * \brief Mapping of command line -a parameter string values to enum values
		 */
		static const std::map<std::string, ESearchAlgorithm, utils::string::icmp> strToAlgorithmMapping
		{
			std::make_pair("BMH", ESearchAlgorithm::BMH),
			std::make_pair("KMP", ESearchAlgorithm::KMP),
			std::make_pair("", ESearchAlgorithm::default),
		};
		/**
		 * \brief Convert string to algorithm type enum value
		 * \param[in] str String to convert
		 * \return Matching algorithm type or default value if not found
		 */
		ESearchAlgorithm strToAlgorithmType(const std::string& str);
		/**
		 * \brief Convert string to algorithm type enum value
		 * \param[out] type Matching algorithm type if found
		 * \param[in] str String to convert
		 * \return true if matching type was found, false otherwise
		 */
		bool strToAlgorithmType(ESearchAlgorithm& type, const std::string& str);


		/**
		 * \brief Mapping of command line flags to enum values
		 */
		// NOTE: do not use case insensitive comparison for flags
		static const std::map<std::string, EExecutionStrategy> strToExecutionStrategyMapping
		{
			std::make_pair("sf", EExecutionStrategy::serialFiles),
			std::make_pair("pf", EExecutionStrategy::parallelFiles),
			std::make_pair("pw", EExecutionStrategy::parallelWindows),
			std::make_pair("", EExecutionStrategy::default),
		};
		/**
		 * \brief Convert string to execution strategy enum value
		 * \param[in] str String to convert
		 * \return Matching enum value or default value if not found
		 */
		EExecutionStrategy strToExecutionStrategy(const std::string& str);
		/**
		 * \brief Convert string to execution strategy enum value
		 * \param[out] strategy Matching enum value if found
		 * \param[in] str String to convert
		 * \return true if matching value was found, false otherwise
		 */
		bool strToExecutionStrategy(EExecutionStrategy& strategy, const std::string& str);

	}
}

#endif // _H_5AC1F24D_5758_4DE7_AABA_21773F9B354A_

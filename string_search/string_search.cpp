#include "stdafx.h"
#include "file_search.h"
#include "params.h"
#include "utils.h"

#include <3rd_party\argh\argh.h>

#include <exception>
#include <iostream>

void printHelp()
{
	std::cout <<
		"usage:" << std::endl <<
		"\t<source_path> <pattern> [-sf|pf|pw]" << std::endl << std::endl <<
		"\t-s <source_path> -p <pattern> [-a <algorithm>] [-e <sampling_count>] [-j <tasks_count>] [-sf|pf|pw]" << std::endl << std::endl <<
		"source_path" << std::endl <<
		"\tfile or directory to search in" << std::endl <<
		"\tWARNINIG: Do not leave '\\' at the end of a quoted path as it is considered as an escape sequence and parameters won't be processed correctly" << std::endl << std::endl <<
		"pattern" << std::endl <<
		"\tsearched pattern" << std::endl << std::endl <<
		"algorithm" << std::endl <<
		"\tsearch algorithm, possible values are:" << std::endl <<
		"\t\t\"KMP\" - Knuth-Morris-Pratt" << std::endl <<
		"\t\t\"BMH\" (default) - Boyer-Moore-Horspool" << std::endl << std::endl <<
		"sampling_count" << std::endl <<
		"\tspecifies how many times the search process should run" << std::endl <<
		"\tdefault value is 1" << std::endl << std::endl <<
		"tasks_count" << std::endl <<
		"\tspecifies number of concurrent task for -pf or -pw flags" << std::endl <<
		"\tdefault value is 0, which means auto and should run one task for each CPU logical core" << std::endl << std::endl <<
		"execution strategy flags:" << std::endl <<
		"\t-sf (default) - serial files - runs one task for each file one by one" << std::endl <<
		"\t-pf - parallel files - runs N tasks (specified by \"-j\" parameter) in parallel each for one file (process multiple files by N tasks)" << std::endl <<
		"\t-pw - parallel windows - runs N tasks (specified by \"-j\" parameter) for each file processed one by one (process each file by N tasks)" << std::endl;
}

void handleException()
{
	try {
		throw;
	}
	catch (const std::exception& e) {
		std::cerr << "Exception occured: " << e.what() << std::endl;
	}
	catch (...) {
		std::cerr << "Unknown exception occured. World is not perfect." << std::endl;
	}
}

int main(int argc, char* argv[])
{
	try {
		argh::parser cmdLine(argc - 1, argv + 1, argh::parser::PREFER_PARAM_FOR_UNREG_OPTION);

		if (cmdLine[{"h", "help"}])
		{	// help is required
			printHelp();
		}
		else
		{	// process params
			stringSearch::SSearchParams params;

			if (cmdLine.params().empty())
			{	// try positional params
				const auto& args{ cmdLine.pos_args() };
				if (2 == args.size()) {
					params.sourcePath.assign(args[0]);
					params.searchedPattern.assign(args[1]);
				}
			}
			else
			{	// try named params
				cmdLine("s") >> params.sourcePath;
				cmdLine("p") >> params.searchedPattern;
				cmdLine("j") >> params.concurrentTasksCount;
				cmdLine("e") >> params.sampling;
				params.searchAlgorithm = stringSearch::utils::strToAlgorithmType(cmdLine("a").str());
			}

			for (const auto& s : cmdLine.flags())
			{	// try to parse execution strategy flag
				if (stringSearch::utils::strToExecutionStrategy(params.executionStrategy, s)) {
					break;
				}
			}

			if (params.sourcePath.empty() || params.searchedPattern.empty()) {
				printHelp(); // required params are not passed on
			}
			else {
				stringSearch::fileSearch(params); // search
			}
		}
	}
	catch (...) {
		handleException();
	}

	return 0;
}

#ifndef _H_1A93BBFC_0AE7_4114_A1DD_D5A186477919_
#define _H_1A93BBFC_0AE7_4114_A1DD_D5A186477919_

namespace stringSearch {

	/**
	 * \brief Defines how files are processed
	 */
	enum class EExecutionStrategy
	{
		serialFiles,		//!< one task processing files in serial
		parallelFiles,		//!< N parallel tasks processing files, each task processing files in serial
		parallelWindows,	//!< N parallel tasks processing file divided to N parts, each task processing one part
		default = serialFiles
	};

}

#endif // _H_1A93BBFC_0AE7_4114_A1DD_D5A186477919_

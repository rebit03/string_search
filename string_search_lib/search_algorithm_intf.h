#ifndef _H_4CBA10B7_EBD5_4127_B247_4B86E6C529F9_
#define _H_4CBA10B7_EBD5_4127_B247_4B86E6C529F9_

#include "types.h"

#include <istream>
#include <string>

namespace stringSearch {
	namespace algorithms {

		class ISearchAlgorithm
		{
		public:
			virtual ~ISearchAlgorithm() {}

			/**
			 * \brief Search within specified size from current position in the stream
			 * \param[in] is Input stream seeked to the required position
			 * \param[in] searchSize Required size to search within
			 * \param[in] pattern Searched pattern
			 * \param[in] presuffixLen Length of prefix and suffix of found pattern
			 * \return Collection of found occurences of searched pattern
			 */
			virtual searchResults_t operator()(std::istream& is, std::streamsize searchSize, const std::string& pattern, std::streamsize presuffixLen) = 0;

			/**
			 * \brief Search preinitialized pattern from current position in the stream to the end of the stream
			 * \param[in] is Input stream seeked to the required position
			 * \param[in] searchSize Required size to search within
			 * \param[in] presuffixLen Length of prefix and suffix of found pattern
			 * \return Collection of found occurences of searched pattern
			 */
			virtual searchResults_t operator()(std::istream& is, std::streamsize searchSize, std::streamsize presuffixLen) = 0;
		};

	}
}

#endif // _H_4CBA10B7_EBD5_4127_B247_4B86E6C529F9_

#ifndef _H_740B619B_6735_4715_824A_0497294DB858_
#define _H_740B619B_6735_4715_824A_0497294DB858_

#include "search_algorithm_intf.h"

#include <unordered_map>

namespace stringSearch {
	namespace algorithms {

		/**
		 * Implementation of Boyer-Moore-Horspool pattern search algorithm
		 */
		class CBoyerMooreHorspool : public ISearchAlgorithm
		{
		public:
			CBoyerMooreHorspool() = default;
			explicit CBoyerMooreHorspool(const std::string& pattern);

			// ISearchAlgorithm
			searchResults_t operator()(std::istream& is, std::streamsize searchSize, const std::string& pattern, std::streamsize presuffixLen) override;
			searchResults_t operator()(std::istream& is, std::streamsize searchSize, std::streamsize presuffixLen) override;
		private:
			using skipTable_t = std::unordered_map<char, size_t>;
			static skipTable_t initSkipTable(const std::string& pattern);
		private:
			std::string		m_pattern;
			skipTable_t		m_skipTable;
		};

	}
}

#endif // _H_740B619B_6735_4715_824A_0497294DB858_

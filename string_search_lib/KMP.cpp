#include "stdafx.h"
#include "KMP.h"

#include <utils_lib\stream_utils.h>

#include <algorithm>

namespace stringSearch {
	namespace algorithms {

		using namespace std;
		namespace streamUtils = stringSearch::utils::stream;

		CKnuthMorrisPratt::CKnuthMorrisPratt(const string& pattern)
			: m_pattern(pattern)
			, m_skipTable(initSkipTable(pattern))
		{
		}

		CKnuthMorrisPratt::skipTable_t CKnuthMorrisPratt::initSkipTable(const string& pattern)
		{
			if (pattern.empty())
			{	// nothing to process, return empty table
				return skipTable_t();
			}

			const size_t count{ pattern.size() };
			skipTable_t skipTable(count + 1);

			skipTable[0] = -1;
			for (size_t i{ 0 }; i < count; ++i) {
				skipTable[i + 1] = skipTable[i];
				while (skipTable[i + 1] > -1 && pattern.at(skipTable[i + 1]) != pattern.at(i)) {
					skipTable[i + 1] = skipTable[skipTable[i + 1]];
				}
				skipTable[i + 1]++;
			}

			return skipTable;
		}

		searchResults_t CKnuthMorrisPratt::operator()(istream& is, streamsize searchSize, const string& pattern, streamsize presuffixLen)
		{
			m_pattern = pattern;
			m_skipTable = initSkipTable(pattern);

			return operator()(is, searchSize, presuffixLen);
		}

		searchResults_t CKnuthMorrisPratt::operator()(istream& is, streamsize searchSize, streamsize presuffixLen)
		{
			is.exceptions(ios::ios_base::badbit); // throw exceptions for I/O operations

			const size_t patternLen{ m_pattern.size() };
			const streamsize inputLen{ streamUtils::getStreamSize(is) };

			streampos start{ is.tellg() };
			searchSize = min((inputLen - start), searchSize);

			searchResults_t results;

			if (static_cast<size_t>(searchSize) >= patternLen)
			{	// input stream is large enough
				int32_t patternPosition{ 0 };
				for (streamsize i{ 0 }; i < searchSize; ++i)
				{
					const char c{ static_cast<char>(is.get()) };

					while ((patternPosition > -1) && (m_pattern.at(patternPosition) != c))
					{
						const int32_t skip{ m_skipTable.at(patternPosition) };
						start += patternPosition - skip;
						patternPosition = skip;
					}

					patternPosition++;

					if (patternPosition == patternLen)
					{	// match
						const streampos offset{ start + static_cast<streampos>(patternLen) };
						results.push_back({
							streamUtils::getPrefix(is, start, presuffixLen),
							streamUtils::getSuffix(is, inputLen, offset, presuffixLen),
							start
						});
						is.seekg(offset);

						const int32_t skip{ m_skipTable.at(patternPosition) };
						start += patternPosition - skip;
						patternPosition = skip;
					}
				}
			}

			return results;
		}

	}
}

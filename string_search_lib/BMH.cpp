#include "stdafx.h"
#include "BMH.h"

#include <utils_lib\stream_utils.h>

#include <algorithm>

namespace stringSearch {
	namespace algorithms {

		using namespace std;
		namespace streamUtils = stringSearch::utils::stream;

		CBoyerMooreHorspool::CBoyerMooreHorspool(const string& pattern)
			: m_pattern(pattern)
			, m_skipTable(initSkipTable(pattern))
		{
		}

		CBoyerMooreHorspool::skipTable_t CBoyerMooreHorspool::initSkipTable(const string& pattern)
		{
			skipTable_t skipTable;

			if (!pattern.empty())
			{	// pattern is not empty
				const size_t count{ pattern.size() - 1 };
				for (size_t i{ 0 }; i < count; ++i) {
					skipTable[pattern.at(i)] = count - i;
				}
			}

			return skipTable;
		}

		searchResults_t CBoyerMooreHorspool::operator()(istream& is, streamsize searchSize, const string& pattern, streamsize presuffixLen)
		{
			m_pattern = pattern;
			m_skipTable = initSkipTable(pattern);

			return operator()(is, searchSize, presuffixLen);
		}

		searchResults_t CBoyerMooreHorspool::operator()(istream& is, streamsize searchSize, streamsize presuffixLen)
		{
			is.exceptions(ios::ios_base::badbit); // throw exceptions for I/O operations

			const size_t patternLen{ m_pattern.size() };
			const streamsize inputLen{ streamUtils::getStreamSize(is) };

			streampos start{ is.tellg() };
			const streampos end = min<streampos>(inputLen, (start + searchSize));

			searchResults_t results;

			if (static_cast<size_t>(end - start) >= patternLen)
			{	// input stream is large enough
				// pattern len is substracted to prevent reading out of the bounds, because it's the size of the buffer which is read from the input stream
				const streampos searchEnd{ end - static_cast<streampos>(patternLen) };
				while (start <= searchEnd)
				{
					string buffer(patternLen, 0);
					is.read(&buffer[0], patternLen);

					size_t j = patternLen - 1;
					while (m_pattern[j] == buffer[j])
					{
						if (0 == j)
						{	// match
							results.push_back({
								streamUtils::getPrefix(is, start, presuffixLen),
								streamUtils::getSuffix(is, inputLen, start + static_cast<streampos>(patternLen), presuffixLen),
								start
							});
							break;
						}
						else {
							j--;
						}
					}

					const char c{ buffer.at(patternLen - 1) };
					if (m_skipTable.find(c) != m_skipTable.end()) {
						start += m_skipTable[c];
					}
					else {
						start += patternLen;
					}
					is.seekg(start);
				}
			}

			return results;
		}

	}
}

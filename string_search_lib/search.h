#ifndef _H_35D287C9_5126_4911_9A25_24278F58CEF4_
#define _H_35D287C9_5126_4911_9A25_24278F58CEF4_

#include "types.h"

#include <istream>
#include <memory>
#include <string>

namespace stringSearch {

	const std::streamsize defaultPrefixSuffixLenght = 3;

	/**
	 * \brief Search class interface
	 */
	class ISearch
	{
	public:
		virtual ~ISearch() {}

		///////////////////////////////////////////////////////////////////////////////////////////
		// pattern initialized class search

		/**
		 * \brief Search preinitialized pattern from current position in the stream to the end of the stream
		 * \param[in] is Input stream seeked to the required position
		 * \param[in] presuffixLen Length of prefix and suffix of found pattern
		 * \return Collection of found occurences of searched pattern
		 */
		virtual searchResults_t search(
			std::istream& is,
			std::streamsize presuffixLen = defaultPrefixSuffixLenght) = 0;

		/**
		 * \brief Search preinitialized pattern from current position in the stream to the end of the stream
		 * \param[out] results Collection of found occurences of searched pattern
		 * \param[in] is Input stream seeked to the required position
		 * \param[in] presuffixLen Length of prefix and suffix of found pattern
		 */
		virtual void search(
			searchResults_t& results,
			std::istream& is,
			std::streamsize presuffixLen = defaultPrefixSuffixLenght) = 0;


		/**
		 * \brief Search preinitialized pattern within specified size from current position in the stream
		 * \param[in] is Input stream seeked to the required position
		 * \param[in] searchSize Required size to search within
		 * \param[in] presuffixLen Length of prefix and suffix of found pattern
		 * \return Collection of found occurences of searched pattern
		 */
		virtual searchResults_t searchRange(
			std::istream& is,
			std::streamsize searchSize,
			std::streamsize presuffixLen = defaultPrefixSuffixLenght) = 0;

		/**
		 * \brief Search preinitialized pattern within specified size from current position in the stream
		 * \param[out] results Collection of found occurences of searched pattern
		 * \param[in] is Input stream seeked to the required position
		 * \param[in] searchSize Required size to search within
		 * \param[in] presuffixLen Length of prefix and suffix of found pattern
		 */
		virtual void searchRange(
			searchResults_t& results,
			std::istream& is,
			std::streamsize searchSize,
			std::streamsize presuffixLen = defaultPrefixSuffixLenght) = 0;

		//
		///////////////////////////////////////////////////////////////////////////////////////////
		// default initialized class search

		/**
		 * \brief Search from current position in the stream to the end of the stream
		 * \param[in] is Input stream seeked to the required position
		 * \param[in] pattern Searched pattern
		 * \param[in] presuffixLen Length of prefix and suffix of found pattern
		 * \return Collection of found occurences of searched pattern
		 */
		virtual searchResults_t search(
			std::istream& is,
			const std::string& pattern,
			std::streamsize presuffixLen = defaultPrefixSuffixLenght) = 0;

		/**
		 * \brief Search from current position in the stream to the end of the stream
		 * \param[out] results Collection of found occurences of searched pattern
		 * \param[in] is Input stream seeked to the required position
		 * \param[in] pattern Searched pattern
		 * \param[in] presuffixLen Length of prefix and suffix of found pattern
		 */
		virtual void search(
			searchResults_t& results,
			std::istream& is,
			const std::string& pattern,
			std::streamsize presuffixLen = defaultPrefixSuffixLenght) = 0;


		/**
		 * \brief Search within specified size from current position in the stream
		 * \param[in] is Input stream seeked to the required position
		 * \param[in] searchSize Required size to search within
		 * \param[in] pattern Searched pattern
		 * \param[in] presuffixLen Length of prefix and suffix of found pattern
		 * \return Collection of found occurences of searched pattern
		 */
		virtual searchResults_t searchRange(
			std::istream& is,
			std::streamsize searchSize,
			const std::string& pattern,
			std::streamsize presuffixLen = defaultPrefixSuffixLenght) = 0;

		/**
		 * \brief Search within specified size from current position in the stream
		 * \param[out] results Collection of found occurences of searched pattern
		 * \param[in] is Input stream seeked to the required position
		 * \param[in] searchSize Required size to search within
		 * \param[in] pattern Searched pattern
		 * \param[in] presuffixLen Length of prefix and suffix of found pattern
		 */
		virtual void searchRange(
			searchResults_t& results,
			std::istream& is,
			std::streamsize searchSize,
			const std::string& pattern,
			std::streamsize presuffixLen = defaultPrefixSuffixLenght) = 0;

		//
		///////////////////////////////////////////////////////////////////////////////////////////
	};

	/**
	 * \brief Returns search class implementing required search algorithm
	 * \param[in] type Type of required algorithm
	 * \param[in] pattern Pattern to initialize search class with
	 * \return Pointer to search class implementation
	 */
	std::unique_ptr<ISearch> getSearchClass(ESearchAlgorithm type, const std::string& pattern = "");

}

#endif // _H_35D287C9_5126_4911_9A25_24278F58CEF4_

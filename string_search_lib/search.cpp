#include "stdafx.h"
#include "search.h"
#include "BMH.h"
#include "KMP.h"

#include "utils_lib\stream_utils.h"

namespace stringSearch {

	using namespace std;

	namespace {

		template <class A>
		class CSearch : public ISearch
		{
		public:
			explicit CSearch(const string& pattern)
				: m_alg(pattern)
			{}

			searchResults_t search(
				istream& is,
				streamsize presuffixLen = defaultPrefixSuffixLenght) override
			{
				const streamsize searchSize{ utils::stream::getStreamSize(is) };
				return m_alg(is, searchSize, presuffixLen);
			}

			void search(
				searchResults_t& results,
				istream& is,
				streamsize presuffixLen = defaultPrefixSuffixLenght) override
			{
				searchResults_t r{ search(is, presuffixLen) };
				results.insert(results.end(), make_move_iterator(r.begin()), make_move_iterator(r.end()));
			}


			searchResults_t searchRange(
				istream& is,
				streamsize searchSize,
				streamsize presuffixLen = defaultPrefixSuffixLenght) override
			{
				return m_alg(is, searchSize, presuffixLen);
			}

			void searchRange(
				searchResults_t& results,
				istream& is,
				streamsize searchSize,
				streamsize presuffixLen = defaultPrefixSuffixLenght) override
			{
				searchResults_t r{ searchRange(is, searchSize, presuffixLen) };
				results.insert(results.end(), make_move_iterator(r.begin()), make_move_iterator(r.end()));
			}


			searchResults_t search(
				istream& is,
				const string& pattern,
				streamsize presuffixLen = defaultPrefixSuffixLenght) override
			{
				const streamsize searchSize{ utils::stream::getStreamSize(is) };
				return m_alg(is, searchSize, pattern, presuffixLen);
			}

			void search(
				searchResults_t& results,
				istream& is,
				const string& pattern,
				streamsize presuffixLen = defaultPrefixSuffixLenght) override
			{
				searchResults_t r{ search(is, pattern, presuffixLen) };
				results.insert(results.end(), make_move_iterator(r.begin()), make_move_iterator(r.end()));
			}


			searchResults_t searchRange(
				istream& is,
				streamsize searchSize,
				const string& pattern,
				streamsize presuffixLen = defaultPrefixSuffixLenght) override
			{
				return m_alg(is, searchSize, pattern, presuffixLen);
			}

			void searchRange(
				searchResults_t& results,
				istream& is,
				streamsize searchSize,
				const string& pattern,
				streamsize presuffixLen = defaultPrefixSuffixLenght) override
			{
				searchResults_t r{ searchRange(is, searchSize, pattern, presuffixLen) };
				results.insert(results.end(), make_move_iterator(r.begin()), make_move_iterator(r.end()));
			}
		private:
			A m_alg;
		};

		template <class algT>
		unique_ptr<ISearch> createSearchClass(const string& pattern)
		{
			return move(make_unique<CSearch<algT>>(pattern));
		}

	} // namespace

	unique_ptr<ISearch> getSearchClass(ESearchAlgorithm type, const string& pattern/* = ""*/)
	{
		switch (type) {
			default:
			case ESearchAlgorithm::BMH:
				return createSearchClass<algorithms::CBoyerMooreHorspool>(pattern);
			case ESearchAlgorithm::KMP:
				return createSearchClass<algorithms::CKnuthMorrisPratt>(pattern);
			break;
		}
	}

}

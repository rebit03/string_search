#ifndef _H_82541E51_5FD5_4BDC_8396_F79A8BDB159F_
#define _H_82541E51_5FD5_4BDC_8396_F79A8BDB159F_

#include <ios>
#include <string>
#include <vector>

namespace stringSearch {

	/**
	 * \brief Type of search algorithm
	 */
	enum class ESearchAlgorithm
	{
		BMH, //!< Boyer-Moore-Horspool
		KMP, //!< Knuth-Morris-Pratt
		default = BMH
	};

	/**
	 * \brief Search result
	 */
	struct SResult
	{
		std::string		prefix; //!< prefix of found pattern
		std::string		suffix; //!< suffix of found pattern
		std::streampos	offset; //!< offset within the stream of found pattern
	};

	/**
	 * \brief Collection of search results
	 */
	using searchResults_t = std::vector<SResult>;

}

#endif // _H_82541E51_5FD5_4BDC_8396_F79A8BDB159F_

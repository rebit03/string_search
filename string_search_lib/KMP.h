#ifndef _H_821F1755_558D_49BA_B2D4_805735A34C1A_
#define _H_821F1755_558D_49BA_B2D4_805735A34C1A_

#include "search_algorithm_intf.h"

#include <vector>

namespace stringSearch {
	namespace algorithms {

		/**
		 * Implementation of Knuth-Morris-Pratt pattern search algorithm
		 */
		class CKnuthMorrisPratt : public ISearchAlgorithm
		{
		public:
			CKnuthMorrisPratt() = default;
			explicit CKnuthMorrisPratt(const std::string& pattern);

			// ISearchAlgorithm
			searchResults_t operator()(std::istream& is, std::streamsize searchSize, const std::string& pattern, std::streamsize presuffixLen) override;
			searchResults_t operator()(std::istream& is, std::streamsize searchSize, std::streamsize presuffixLen) override;
		private:
			using skipTable_t = std::vector<int32_t>;
			static skipTable_t initSkipTable(const std::string& pattern);
		private:
			std::string		m_pattern;
			skipTable_t		m_skipTable;
		};

	}
}

#endif // _H_821F1755_558D_49BA_B2D4_805735A34C1A_
